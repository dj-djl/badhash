import badHash from './badHash.mjs';

const inputs = [`hello`, `world`];
console.log(inputs)
for (const src of inputs) {
  console.log(`after 1 round: ${src} : ${badHash(src, 1)}  after 2 rounds: ${src} : ${badHash(src, 2)}  after 3 rounds: ${src} : ${badHash(src, 3)}`);
}
